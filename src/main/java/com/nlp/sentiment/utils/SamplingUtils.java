package com.nlp.sentiment.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.collections.CollectionUtils;

public class SamplingUtils {

	public static List<Long> reservoir(List<Long> elements, int size) {
		if (CollectionUtils.isEmpty(elements) || elements.size() <= size) {
			return elements;
		}
		final Random random = new Random();
		List<Long> samples = new ArrayList<Long>(size);
		for (int i = 0; i < elements.size(); i++) {
			if (i < size) {
				samples.add(elements.get(i));
			} else {
				int rand = random.nextInt(i);
				if (rand < size) {
					samples.set(rand, elements.get(rand));
				}
			}
		}
		return samples;
	}
}
