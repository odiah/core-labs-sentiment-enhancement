package com.nlp.sentiment.exception;

public class LSISentimentEnhancerException extends SentimentEnhancerException {

	private static final long serialVersionUID = -4204066553587340451L;

	public LSISentimentEnhancerException(Throwable cause) {
		super(cause);
	}

	public LSISentimentEnhancerException(String message) {
		super(message);
	}

	public LSISentimentEnhancerException(Exception e) {
		super(e);
	}
}
