package com.nlp.sentiment.metrics;

public class CosineSimilarity implements Similarity<String> {

	public double similarityScore(String u, String v) {
		CosineDistance distance = new CosineDistance();
		return 1 - distance.getDistance(u, v);
	}

}
