package com.nlp.sentiment.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.beust.jcommander.IStringConverter;

public class QueryConverter implements IStringConverter<List<String>> {

	public List<String> convert(String input) {
		List<String> queries = new ArrayList<String>();
		if (!StringUtils.isEmpty(input)) {
			File file = new File(input);
			LineNumberReader lnr = null;
			String line;
			try {
				lnr = new LineNumberReader(new FileReader(file));
				while ((line = lnr.readLine()) != null) {
					queries.add(line);
				}
			} catch (FileNotFoundException e) {
				return Arrays.asList(input.split(","));
			} catch (IOException e) {
				return Arrays.asList(input.split(","));
			} finally {
				try {
					if (lnr != null) {
						lnr.close();
					}
				} catch (IOException e) {
					return null;
				}
			}
		}
		return queries;
	}
}
