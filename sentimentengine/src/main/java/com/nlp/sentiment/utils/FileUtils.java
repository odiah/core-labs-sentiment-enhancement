package com.nlp.sentiment.utils;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import au.com.bytecode.opencsv.CSVWriter;

public class FileUtils {

	public static void write(String filepath, List<List<String>> rows)
			throws IOException {
		if (CollectionUtils.isEmpty(rows))
			return;
		CSVWriter writer = new CSVWriter(new FileWriter(filepath), ',');
		for (List<String> row : rows) {
			writer.writeNext((String[]) row.toArray(new String[row.size()]));
		}
		writer.close();
	}
}
